package io.github.privacystreams.core;

import android.content.Context;

import io.github.privacystreams.core.exceptions.PSException;
import io.github.privacystreams.core.purposes.Purpose;
import io.github.privacystreams.utils.Logging;
import io.github.privacystreams.utils.PermissionUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * The unified query interface for all kinds of personal data.
 * You will need to construct an UQI with `UQI uqi = new UQI(context);`
 * To get a stream of personal data, simply call `uqi.getData` with a Stream provider.
 */

public interface UQI {
    /**
     * Get a PStream from a provider with a purpose.
     * For example, using `uqi.getData(Contact.getLogs(), Purpose.FEATURE("..."))`
     * will return a stream of contacts.
     * @param pStreamProvider the function to provide the personal data stream,
     *                        e.g. Geolocation.asUpdates().
     * @param purpose the purpose of personal data use, e.g. Purpose.ADS("xxx").
     * @return a multi-item stream
     */
    public PStream getData(PStreamProvider pStreamProvider, Purpose purpose);

    /**
     * Stop all query in this UQI.
     */
    public void stopAll();

    /**
     * Evaluate current UQI.
     *
     * @param query the query to evaluate.
     * @param retry whether to try again if the permission is denied.
     */
    public void evaluate(Function<Void, Void> query, boolean retry);
}