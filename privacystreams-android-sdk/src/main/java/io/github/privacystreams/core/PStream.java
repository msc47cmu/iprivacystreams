package io.github.privacystreams.core;

import java.util.List;
import io.github.privacystreams.core.exceptions.PSException;


/**
 * The interface of PStream (privacy stream).
 * A PStream is a stream containing privacy sensitive items, and each item is an instance of `Item`.
 * A PStream is produced by `uqi.getData` method.
 *
 * It can be transformed to another PStream using transformation functions,
 * such as `filter`, `groupBy`, `map`, etc.
 *
 * Finally, it can be outputted using `asFieldList()`, `count`, etc.
 */
public interface PStream {

    /**
     * Transform the current PStream to another PStream.
     * @param pStreamTransformation the function used to transform the stream
     * @return the transformed stream
     */
    public PStream transform(PStreamTransformation pStreamTransformation);

    /**
     * Output the current PStream.
     * @param pStreamAction the function used to output stream
     */
    public void output(PStreamAction pStreamAction);

    // *****************************
    // Filters
    // Filters are used to include/exclude some items in a stream.

    /**
     * Filter the stream by testing an item with a function.
     * Specifically, keep the items that satisfy the function (aka. the function returns true).
     * Eg. `filter(eq("x", 100))` will keep the items whose x field is equal to 100.
     *
     * @param itemChecker the function to check each item.
     * @return The filtered stream.
     */
    public PStream filter(Function<Item, Boolean> itemChecker);

    /**
     * Filter the stream by checking whether a field equals a value.
     * Specifically, keep the items in which the field equals the given value.
     * Eg. `filter("x", 100)` will keep the items whose x field is equal to 100.
     *
     * @param fieldName the name of field to check
     * @param fieldValue the value to compare with the field
     * @return The filtered stream.
     */
    public <TValue> PStream filter(String fieldName, TValue fieldValue);

    /**
     * Only keep the items that are different from the previous ones in the stream.
     * Eg. a stream [1, 1, 2, 2, 2, 1, 1] will be [1, 2, 1] after `keepChanges()`
     *
     * @return the filtered stream.
     */
    public PStream keepChanges();

    /**
     * Only Keep the items whose fields are different from the previous ones in the stream.
     * Similar to `keepChanges()`, but only monitor a certain field
     *
     * @param fieldName the name of field to check whether an item should be kept
     * @return the filtered stream.
     */
    public PStream keepChanges(String fieldName);

    /**
     * Sample the items based on a given interval. The items sent within the time interval
     * since last item are dropped.
     * Eg. If a stream has items sent at 1ms, 3ms, 7ms, 11ms and 40ms,
     * `sampleByInterval(10)` will only keep the items sent at 1ms, 11ms and 40ms.
     *
     * @param minInterval the minimum interval (in milliseconds) between each two items.
     * @return the filtered stream.
     */
    public PStream sampleByInterval(long minInterval);

    /**
     * Sample the items based on a given step count. The items are filtered to make sure
     * `stepCount` number of items are dropped between each two new items.
     * Eg. `sampleByCount(2)` will keep the 1st, 4th, 7th, 10th, ... items
     *
     * @param stepCount the num of items to drop since last item
     * @return the filtered stream
     */
    public PStream sampleByCount(int stepCount);

    // *****************************
    // Limiters
    // Limiters are used to limit the length of a stream.

    /**
     * Limit the stream by checking each item with a function.
     * Specifically, keep the stream as long as the checker holds (aka. the checker function returns true).
     * Eg. `limit(eq("x", 100))` will keep all items in the stream as long as x field equals to 100,
     * once an item's x value is not equal to 100, the stream stops.
     *
     * @param itemChecker the function to check each item.
     * @return The limited stream.
     */
    public PStream limit(Function<Item, Boolean> itemChecker);

    /**
     * Limit the stream with a max number of items.
     * Specifically, stop the stream if the count of items exceeds the threshold.
     * Eg. `limit(10)` will limit the stream to at most 10 items
     *
     * @param maxCount      the max number of items
     * @return The limited stream.
     */
    public PStream limit(int maxCount);

    /**
     * Limit the stream with a timeout, stop the stream after time out.
     * Eg. `timeout(Duration.seconds(10))` will limit the stream to at most 10 seconds
     *
     * @param timeoutMilliseconds      the timeout milliseconds
     * @return The limited stream.
     */
    public PStream timeout(long timeoutMilliseconds);

    // *****************************
    // Mappers
    // Mappers are used to map each item in the stream to another item

    /**
     * Convert each item in the stream with a function.
     * Eg. `map(ItemOperators.setField("x", 10))` will set the "x" field of each item to 10 in the stream.
     *
     * @param itemConverter      the function to map each item to another item
     * @return The stream with items after mapping
     */
    public PStream map(Function<Item, Item> itemConverter);

    /**
     * Make the items be sent in a fixed interval.
     * Eg. If a stream has items sent at 1ms, 3ms, 7ms, 11ms and 40ms,
     * `inFixedInterval(10)` will send items at 7ms, 11ms, 11ms and 40ms, in a 10ms interval.
     *
     * @param fixedInterval the fixed interval in milliseconds.
     * @return The stream with items after mapping
     */
    public PStream inFixedInterval(long fixedInterval);

    /**
     * Project each item by including some fields.
     * Other fields will be removed.
     * Eg. `project("name", "email")` will only keep the "name" and "email" field in each item.
     *
     * @param fieldsToInclude the fields to include
     * @return The stream with items after projection
     */
    public PStream project(String... fieldsToInclude);

    /**
     * Set a field to a new value for each item in the stream.
     * The value is computed with a function that take the item as input.
     * Eg. `setField("x", Comparators.gt("y", 10))` will set a new boolean field "x" to each item,
     * which indicates whether the "y" field is greater than 10.
     *
     * @param fieldToSet the name of the field to set.
     * @param fieldValueComputer the function to compute the value of the new field based on each item.
     * @param <TValue> the type of the new field value
     * @return the stream of items with the new field set
     */
    public <TValue> PStream setField(String fieldToSet, Function<Item, TValue> fieldValueComputer);

    /**
     * Set a field to a new value for each item in the stream.
     * This transformation can only be used after invoking group methods (`groupBy`, `localGroupBy`).
     * The value is computed with a function that takes the grouped items as input at runtime.
     * Eg. `setGroupField("count", StatisticOperators.count())` will set a new field "count" to each item,
     * which represents the number of items in the grouped sub stream.
     *
     * @param fieldToSet the name of the field to set.
     * @param fieldValueComputer the function to compute the new field value, which takes the list of grouped items as input.
     * @param <TValue> the type of the new field value
     * @return the stream of items with the new field set
     */
    public <TValue> PStream setGroupField(String fieldToSet, Function<List<Item>, TValue> fieldValueComputer);

    /**
     * Set the value of a new field with a value generator function.
     * The value generator function is independent from current item, which does not need a input (input type is Void).
     * The value generator will be evaluated on demand at runtime.
     * Eg. `setIndependentField("time", TimeOperators.getCurrentTime())` will set the field "time" to a timestamp in each item;
     * `setIndependentField("wifiStatus", DeviceOperators.isWifiConnected())` will set the field "wifiStatus" to a boolean indicating whether wifi is connected in each item.
     *
     * @param fieldToSet the name of the field to set.
     * @param valueGenerator the function to compute the field value.
     * @param <TValue> the type of the new field value.
     * @return the stream of items with the new field set
     */
    public <TValue> PStream setIndependentField(String fieldToSet, Function<Void, TValue> valueGenerator);

    // *****************************
    // Mergers
    // Mergers are used to merge the items in the stream and the items in another stream

    /**
     * Union the items in current stream with another stream.
     *
     * @param anotherStreamProvider     the provider of another stream
     * @return The merged stream.
     */
    public PStream union(PStreamProvider anotherStreamProvider);


    // *****************************
    // Reorders
    // Reorders are used to change the order of items in the stream

    /**
     * Sort the items according to the value of a field, in ascending order.
     * Eg. `sortBy("timestamp")` will sort the items in the stream by timestamp field.
     *
     * @param fieldName     the field used to sort the items in current stream, in ascending order
     * @return The stream with sorted items.
     */
    public PStream sortBy(String fieldName);

    /**
     * Shuffle the items.
     * `shuffle()` will randomize the order of the items in the stream.
     *
     * @return The stream with shuffled items.
     */
    public PStream shuffle();

    /**
     * Reverse the order of items
     * `reverse()` will reverse the order of the items in the stream.
     *
     * @return The stream with reversed items.
     */
    public PStream reverse();

    // *****************************
    // StreamGrouper
    // StreamGrouper are used to group some items to one item

    /**
     * Group the items according to a field.
     * After grouping, the items in the new stream will only have two fields.
     * One is the field used for grouping by. Another is "grouped_items" which is a list of grouped Items.
     * Eg. `groupBy("x")` will group the items with same "x" field,
     * and the item in the stream after groupping will contain two fields: "x" and "grouped_items".
     *
     * @param groupField the field used to group the items in current stream.
     * @return The grouped stream
     */
    public PStream groupBy(String groupField);

    /**
     * Group the **contiguous** items according to a field.
     * After grouping, the items in the new stream will only have two fields.
     * One is the field used for grouping by. Another is "grouped_items" which is a list of grouped Items.
     * Eg.  `localGroupBy("x")` will group the contiguous items with same "x" field,
     * and the item in the stream after groupping will contain two fields: "x" and "grouped_items".
     *
     * @param groupField the field used to group the items in current stream.
     * @return The grouped stream
     */
    public PStream localGroupBy(String groupField);

    /**
     * Un-group a list field in each item to multiple items.
     * Each element in the list will be a new field in each item of the new stream.
     * After un-grouping, the items in the new streams will have the same amount of fields
     * as the original stream.
     * However, the list field (`unGroupField`) will be replaced by a new field (`newField`).
     * Eg.  `unGroup("emails", "email")` will un-group the "emails" field (which is a list)
     * in an item to several new items with a "email" field.
     *
     * @param unGroupField the field to un-group, whose value should be a list
     * @param newField the new field name in the new stream
     * @return The un-grouped stream
     */
    public PStream unGroup(String unGroupField, String newField);

    // *****************************
    // Output functions
    // Output functions are used to output the items in a stream

    /**
     * Output the items in the stream with a function, and pass the result to a callback.
     * This method will not be blocked.
     * Eg. `outputItems(StatisticOperators.count(), new Callback<Integer>(){...})`
     * will count the number of items and callback with the number.
     *
     * @param resultComputer the function used to compute result based on the items in current stream
     * @param resultHandler the function to handle the result
     * @param <Tout> the type of the result
     */
    public <Tout> void output(Function<List<Item>, Tout> resultComputer, Callback<Tout> resultHandler);

    /**
     * Output the items in the stream with a function.
     * This method will block until the result returns.
     * Eg. `output(StatisticOperators.count())` will output the number of items.
     *
     * @param itemsCollector the function used to output current stream
     * @param <Tout> the type of the result
     * @return the result
     * @throws PSException if failed to the result.
     */
    public <Tout> Tout output(Function<List<Item>, Tout> itemsCollector) throws PSException;

    /**
     * Get the first item in the stream.
     *
     * @return the first item of current PStream
     */
    public Item getFirst();

    /**
     * Get the first value of the given field in the stream.
     *
     * @return the first item of current PStream
     */
    public <TValue> TValue getFirst(String fieldName);

    /**
     * Pick the N-th item in the stream. N is the index.
     *
     * @param index the index of target item.
     * @return the item selected from the current PStream
     */
    public Item getItemAt(int index) throws PSException;

    /**
     * Get the N-th value of a given field. N is the index.
     *
     * @param fieldName the name of the field to select
     * @param index the index of target item.
     * @return the item selected from the current PStream
     */
    public <TValue> TValue getFieldAt(String fieldName, int index) throws PSException;

    /**
     * Do nothing with the items.
     */
    public void idle();

    /**
     * Print the items for debugging.
     */
    public void debug();

    /**
     * Print the items in current stream.
     *
     * @param logTag the log tag to use in printing current stream
     */
    public PStream logAs(String logTag);

    /**
     * Print the items in current stream over socket.
     *
     * @param logTag the log tag to use in printing current stream
     */
    public PStream logOverSocket(String logTag);

    /**
     * Count the number of items.
     *
     * @return the count of number of items in the stream.
     */
    public int count() throws PSException;

    /**
     * Collect the items in the stream to a list.
     * Each item in the list is an instance of `Item`.
     *
     * @return a list of key-value maps, each map represents an item
     */
    public List<Item> asList() throws PSException;

    /**
     * Select a field in each item and output the field values to a list.
     *
     * @param fieldToSelect the field to select
     * @param <TValue> the type of field value
     * @return a list of field values
     */
    public <TValue> List<TValue> asList(String fieldToSelect) throws PSException;

    /**
     * Callback with each item.
     *
     * @param callback the callback to invoke for each item.
     */
    public void forEach(Function<Item, Void> callback);

    /**
     * Callback with a certain field of each item.
     *
     * @param fieldToSelect the name of the field to callback with
     * @param callback the callback to invoke for each item field
     * @param <TValue> the type of the field
     */
    public <TValue> void forEach(String fieldToSelect, Function<TValue, Void> callback);

    /**
     * Watch for changes in field state. When field changes, the updated state is sent back.
     *
     * @param field the field to watch for changes
     */
    public void observe(String field);

    /**
     * Callback with an item once one item is present.
     *
     * @param callback the callback to invoke once the item is present
     */
    public void ifPresent(Function<Item, Void> callback);

    /**
     * Callback with a field value of an item once the field value is present.
     *
     * @param fieldToSelect the name of the field to callback with
     * @param callback the callback to invoke once the field value is present
     * @param <TValue> the type of the field
     */
    public <TValue> void ifPresent(String fieldToSelect, Function<TValue, Void> callback);

    /**
     * Reuse current stream.
     *
     * @param numOfReuses number of reuses
     * @return the stream ready for reuse
     */
    public PStream reuse(int numOfReuses);

    /**
     * Get a value generator that can be evaluated on demand.
     * The generator will not be evaluated immediately, instead, it will be evaluated once `apply()` is called.
     *
     * @param streamOutputter the function to output the stream
     * @return the value generator
     */
    public <Tout> Function<Void, Tout> getValueGenerator(Function<PStream, Tout> streamOutputter);

}