package io.github.privacystreams.core;

import io.github.privacystreams.core.exceptions.PSException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The abstraction of all function in PrivacyStreams.
 * A Function converts a input in type `Tin` to a output in type `Tout`.
 */

public interface Function<Tin, Tout> {
    public Set<String> getRequiredPermissions();

    /**
     * Apply this function
     * @param uqi the instance of UQI
     * @param input the function input
     * @return the function output
     */
    public Tout apply(UQI uqi, Tin input);


    /**
     * Compound this function with another function
     * @param function another function
     * @param <Ttemp> the intermediate variable type between two functions
     * @return the compound function
     */
    public <Ttemp> Function<Tin, Ttemp> compound(Function<Tout, Ttemp> function);

    public String toString();
    public Function<Tin, ?> getHead();
    public Function<?, Tout> getTail();
}